from entity import Entity


class Account(Entity):
    """ Account entity """

    def __init__(self, name, **properties):
        Entity.__init__(self, 'account', name, **properties)
