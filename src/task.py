from entity import Entity
import arrow
import click


class Task(Entity):
    """ Task entity """

    def __init__(self, name, **properties):
        self._due = None
        self._recurrence = None
        self._priority = 'low'
        self._memo = None
        self._completed = None

        Entity.__init__(self, 'task', name, **properties)

    @property
    def due(self):
        return self._due

    @due.setter
    def due(self, due):
        try:
            self._due = arrow.get(due)
        except ValueError as e:
            click.secho('Invalid completed date, expected format is Y-m-d', fg='red')
            raise e

    @property
    def completed(self):
        return self._completed

    @completed.setter
    def completed(self, completed):
        try:
            self._completed = arrow.get(completed)
        except ValueError as e:
            click.secho('Invalid completed date, expected format is Y-m-d', fg='red')
            raise e

    def done(self):
        if (self._completed is None):
            self._completed = arrow.now()

    @property
    def recurrence(self):
        return self._recurrence

    @recurrence.setter
    def recurrence(self, recurrence):
        if (recurrence in ['hourly', 'daily', 'monthly', 'yearly']):
            self._recurrence = recurrence
        else:
            click.secho('Invalid recurrence, expected values are: hourly, daily, monthly, yearly', fg='red')
            raise ValueError('Invalid recurrence, expected values are: hourly, daily, monthly, yearly')

    @property
    def priority(self):
        return self._priority

    @priority.setter
    def priority(self, priority):
        if (priority in ['immediate', 'high', 'medium', 'low']):
            self._priority = priority
        else:
            click.secho('Invalid priority, expected values are: immediate, high, medium, low', fg='red')
            raise ValueError('Invalid priority, expected values are: immediate, high, medium, low')

    @property
    def memo(self):
        return self._memo

    @memo.setter
    def memo(self, memo):
        self._memo = memo
