from distutils.core import setup

setup(name='clerk',
      version='0.1',
      description='A command-line every day life assistant (todo, calendar, personal finance, etc.)',
      url='https://github.com/giann/clerk',
      author='Benoit Giannangeli',
      author_email='giann008@gmail.com',
      license='MIT',
      install_requires=[
          'arrow',
          'click'
      ],
      zip_safe=False)