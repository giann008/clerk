from entity import Entity


class Category(Entity):
    """ Category entity """

    def __init__(self, name, **properties):
        self._account = None

        Entity.__init__(self, 'category', name, **properties)

        # Category id is it's name
        self._id = name

    @property
    def account(self):
        return self._account

    @account.setter
    def account(self, account):
        self._account = account