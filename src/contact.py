from entity import Entity
import arrow
import click


class Contact(Entity):
    """ Contact entity """

    def __init__(self, name, **properties):
        self._address = None
        self._mail = None
        self._phone = None
        self._website = None

        Entity.__init__(self, 'contact', name, **properties)

        self._id = name

    @property
    def address(self):
        return self._address

    @address.setter
    def address(self, address):
        self._address = address

    @property
    def mail(self):
        return self._mail

    @mail.setter
    def mail(self, mail):
        self._mail = mail

    @property
    def phone(self):
        return self._phone

    @phone.setter
    def phone(self, phone):
        self._phone = phone

    @property
    def website(self):
        return self._website

    @website.setter
    def website(self, website):
        self._website = website