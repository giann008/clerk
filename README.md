# clerk
A command-line every day life assistant (todo, calendar, personal finance, etc.)

# Features

## Libs:

  - [click](http://click.pocoo.org/): cli
  - [tablib](http://docs.python-tablib.org/en/latest/): json/yaml/...

## Entities

### Common properties
  - `uid`: string (`type-f12b50ca9be4`)
  - `name`: string
  - `created`: datetime
  - `updated`: datetime
  - **`related`**: [uid, ...]
  - `image` (icon or photo)

### Task
  - `due`: datetime
  - `status`: `(pending|done)`
  - `recurrence`: `(hourly|daily|monthly|yearly)`
  - `priority`: `(immediate|high|medium|low)`
  - `memo`: string (150 words)

### Note
  - `text`: string (markdown)

### Appointment
  - `start`: datetime
  - `end`: datetime
  - `recurrence`: `(hourly|daily|monthly|yearly)`
  - `memo`: string (150 words)

### Project
Unlike tags, projects are exclusive (one project allowed by entity)

### Tag
Tags name are unique and can be used as ids.
  - `color`

### Category
Unlike tags, categories are exclusive (one category allowed by entity)

### External/Contact
Can be a shop, the government (taxes), an administration or a person. Any external entity you would interact with.
  - `address`
  - `mail`
  - `phone`
  - `website`

### Bank account
  - `url`
  - `balance`: float

### Transaction
  - `cleared`: datetime (`null` = not cleared)
  - `amount`: float
  - `payee`: uid (external or bank account)
  - `payer`: uid (external or account)
  - `category`: uid

### Budget
If no dates are set, it can be used to make forecast budget
  - `start`: datetime
  - `end`: datetime (would be set to 1st to 1st of the next month)
  - `lines`: `[{category, amount}]`

### Documents
  - `path`
  - `meta`

### Location
  - `(lat, lon)`

### Goals
Somewhat like tasks but long term


## Command syntax

### Add
`clerk add TYPE NAME [property:value]`

`TYPE`:
- `(task|tsk)`
- `(note|n)`
- `(appointment|a)`
- `(project|p)`
- `(tag|t)`
- `(category|cat)`
- `(contact|c)`
- `(account|acc)`
- `(transaction|tr)`
- `(budget|b)`
- `(document|d)`
- `(location|l)`
- `(goal|g)`

### Delete
`clerk delete ID`

### Modify
`clerk modify ID [property:value]`

### Reports
List, budget reports, user defined reports, etc.