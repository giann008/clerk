from jsonpickle import encode, decode
import click

from entity import Entity
from note import Note
from tag import Tag
from task import Task
from contact import Contact
from account import Account
from transaction import Transaction
from category import Category
from budget import Budget


class Collection(object):
    """ Hold all entities """

    def __init__(self):
        self._entities = {}

    @property
    def entities(self):
        return self._entities

    def add_entity(self, type, name, **properties):
        try:
            entity = eval(type.capitalize())(name, **properties)

            # user readable id
            if (entity.id is None):
                entity.id = self.next_id()

            if (self.get_entity(entity.id) is not None):
                raise Exception('Entity \'' + entity.id + '\' already exists')

            self._entities[entity.id] = entity

            click.echo(entity.__class__.__name__ + ' #' + str(entity.id) + ' created')
            # click.secho(encode(entity), fg='red')

            return entity
        except Exception as e:
            click.secho('Entity could not be created', fg='red')
            print(str(e))

            return None

    def delete_entity(self, id):
        if (id in self._entities.keys()):
            self._entities.pop(id, None)

            click.secho('Entity #' + str(id) + ' deleted', fg='blue')
        else:
            click.secho('Entity #' + str(id) + ' does not exist', fg='red')

    def get_entity(self, id):
        if (id in self._entities.keys()):
            return self._entities[id]
        else:
            return None

    def get_entities(self, cls):
        entities = []
        for entity in self.entities.values():
            if (isinstance(entity, cls)):
                entities.append(entity)

        return entities

    def modify_entity(self, id, **properties):
        if (id in self._entities.keys()):
            self._entities[id].update(**properties)
            click.secho('Entity #' + str(id) + ' updated', fg='green')
            # click.secho(encode(self._entities[id]), fg='red')
        else:
            click.secho('Entity #' + str(id) + ' does not exist', fg='red')

    def to_json(self):
        return encode(self)

    @staticmethod
    def from_json(path):
        file = open(path, 'r', 1)

        json = file.read()

        file.close()

        return decode(json)

    def save(self):
        file = open('clerk.json', 'w', 1)

        file.write(self.to_json())

        file.close()

        click.secho('Collection saved', fg='white')

    def next_id(self):
        nid = 0

        for entity in self.entities.values():
            if (isinstance(entity.id, int) and entity.id > nid):
                nid = entity.id

        return nid + 1
