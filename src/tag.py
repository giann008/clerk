from entity import Entity


class Tag(Entity):
    """ Tag entity """

    def __init__(self, name, **properties):
        Entity.__init__(self, 'tag', name, **properties)

        # Tag id is it's name
        self._id = name
