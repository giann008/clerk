from uuid import uuid4
import arrow
from jsonpickle import encode, decode
from utils import get_instance_properties
import click


class Entity(object):
    """ Basic entity """

    def __init__(self, type, name, **properties):
        self._type = type
        self._name = name

        # random
        self._uid = type + '-' + str(uuid4())
        self._id = None

        self._created = arrow.now()
        self._updated = arrow.now()

        self._image = None

        self._related = set()

        self.update(**properties)

    def update(self, **properties):
        instance_properties = get_instance_properties(self)

        modified = False
        for property in properties.keys():
            if (property in instance_properties):
                setattr(self, property, properties[property])
                modified = True
            else:
                click.secho('Property \'' + property + '\' doesn\'t match', fg='red')
                raise ValueError('Property ' + property + ' doesn\'t match')

        if (modified):
            self._updated = arrow.now()

    def match(self, **properties):
        instance_properties = get_instance_properties(self)

        for property in properties.keys():
            if (property in instance_properties and getattr(self, property) != properties[property]):
                return False
        
        return True

    def to_array(self):
        array = []
        properties = get_instance_properties(self)

        for property in properties:
            attr = getattr(self, property)

            if (isinstance(attr, arrow.Arrow)):
                attr = attr.humanize()

            array.append(str(attr))

        return array

    @property
    def type(self):
        return self._type

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    @property
    def uid(self):
        return self._uid

    @property
    def created(self):
        return self._created

    @property
    def updated(self):
        return self._updated

    @updated.setter
    def updated(self, updated):

        self._updated = updated

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, image):

        self._image = image

    @property
    def related(self):
        return self._related

    @related.setter
    def related(self, related):

        self._related = related

    def add_related(self, uid):
        self._related.add(uid)

    def to_json(self):
        return encode(self)

    @staticmethod
    def from_json(json):
        return decode(json)
