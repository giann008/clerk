from entity import Entity
import arrow
import click


class Budget(Entity):
    """ Budget entity """

    def __init__(self, name, **properties):
        self._start = arrow.now().floor('month')
        self._end = arrow.now().ceil('month')
        self._lines = {}

        Entity.__init__(self, 'budget', name, **properties)

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, start):
        self._start = arrow.get(start)

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, end):
        self._end = arrow.get(end)

    @property
    def lines(self):
        return self._lines

    def budget(self, category, amount):
        self.lines[category] = amount