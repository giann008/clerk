from entity import Entity
import arrow
import click


class Transaction(Entity):
    """ Transaction entity """

    def __init__(self, name, **properties):
        self._cleared = None
        self._amount = 0.0
        self._payee = None
        self._payer = None
        self._category = None
        self._working_date = arrow.now()
        self._memo = ''

        Entity.__init__(self, 'transaction', name, **properties)

        if (self._payee is None):
            click.secho('A payee is required', fg='red')
            raise TypeError('A payee is required')

        if (self._payer is None):
            click.secho('A payer is required', fg='red')
            raise TypeError('A payer is required')

    @property
    def cleared(self):
        return self._cleared

    @cleared.setter
    def cleared(self, cleared):
        try:
            self._cleared = arrow.get(cleared) if cleared is not None else None
        except ValueError as e:
            click.secho('Invalid cleared date, expected format is Y-m-d', fg='red')
            raise e

    @property
    def working_date(self):
        return self._working_date

    @working_date.setter
    def working_date(self, working_date):
        try:
            self._working_date = arrow.get(working_date)
        except ValueError as e:
            click.secho('Invalid working date, expected format is Y-m-d', fg='red')
            raise e

    @property
    def amount(self):
        return self._amount

    @amount.setter
    def amount(self, amount):
        self._amount = int(amount)

    @property
    def payee(self):
        return self._payee

    @payee.setter
    def payee(self, payee):
        self._payee = payee

    @property
    def payer(self):
        return self._payer

    @payer.setter
    def payer(self, payer):
        self._payer = payer

    @property
    def category(self):
        return self._category

    @category.setter
    def category(self, category):
        self._category = category

    @property
    def memo(self):
        return self._memo

    @memo.setter
    def memo(self, memo):
        self._memo = memo