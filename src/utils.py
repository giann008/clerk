from colorclass import Color
import locale


def get_instance_properties(obj):
    return [a for a in dir(obj) if not a.startswith('_') and not callable(getattr(obj, a))]


def format_amount(amount, color=True):
    amount = round(int(amount)/100, 2)

    if (color):
        if (amount > float(0)):
            return Color('{green}' + locale.currency(amount, grouping=True) + '{/green}')
        elif (amount < float(0)):
            return Color('{red}' + locale.currency(amount, grouping=True) + '{/red}')
        else:
            return Color('{yellow}' + locale.currency(amount, grouping=True) + '{/yellow}')
    else:
        return locale.currency(amount, grouping=True)


def currency_to_int(currency):
    return locale.atof(currency.replace('$', '').replace('(', '-').replace(')', ''))


def altern_lines(str):
    lines = str.split('\n')
    ntable = ''

    i = 1
    for line in lines:
        if (i % 2 == 0 and i > 2):
            ntable += ' ' + Color('{bgblack}' + line + '{/bgblack}\n')
        else:
            ntable += ' ' + line + '\n'

        i += 1

    return ntable
