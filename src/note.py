from entity import Entity


class Note(Entity):
    """ Note entity """

    def __init__(self, name, **properties):
        self._text = ''

        Entity.__init__(self, 'note', name, **properties)

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, text):
        self._text = text
